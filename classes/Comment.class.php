<?php

include_once "Db.class.php";

class Comment
{
	private $c_iUserID;
	private $c_iPostID;
	private $c_sComment;
	

	
	public function __set($p_sProperty, $p_vValue)
	{
		switch($p_sProperty)
		{
			case "UserID":
				$this->$c_iUserID = $p_vValue;
				break;
		}
		switch($p_sProperty)
		{
			case "PostID":
				$this->$c_iPostID = $p_vValue;
				break;
		}
		switch($p_sProperty)
		{
			case "Comment":
				$this->c_sComment = $p_vValue;
				break;
		}
	   
	}
	
	public function __get($p_sProperty)
	{
		$vResult = null;
		switch($p_sProperty)
		{
		case "UserID": 
			$vResult = $this->$c_iUserID;
			break;
		}
		switch($p_sProperty)
		{
		case "postID": 
			$vResult = $this->$c_iPostID;
			break;
		}
		switch($p_sProperty)
		{
		case "Comment": 
			$vResult = $this->c_sComment;
			break;
		}

		return $vResult;
	}


	public function Save() {
		$conn = Db::getInstance();
		$statement = $conn->prepare('INSERT INTO comment(comment) VALUES(:comment)');
		//$statement->bindValue(':postID', $this->postID);
		$statement->bindValue(':comment', $this->c_sComment);
		$statement->execute();
	}

	public function GetRecentActivities() {
		$conn = Db::getInstance();
		$allComments = $conn->query("SELECT * FROM comment WHERE postID=:postID ORDER BY id DESC;");
		return $allComments;
	}

	}
	
?>
